# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.14.1":
    raise HTTP(500, "Requires web2py 2.13.3 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# app configuration made easy. Look inside private/appconfig.ini
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
myconf = AppConfig(reload=True)

db = DAL('mysql://root:root123@localhost/pillrdb')

db.define_table('User',
                Field('FirstName', 'string'),
                Field('LastName', 'string'),
                Field('Password', 'string'),
                Field('facebookHandle', 'string'),
                Field('GoogleHandle', 'string'),
                Field('PhoneNumber', 'string', required=True),
                Field('EmailId', 'string', required=True),
                Field('Address1', 'string'),
                Field('Address2', 'string'),
                Field('ZipCode', 'string'),
                Field('City', 'string'),
                Field('State', 'string'),
                Field('Country', 'string'),
                Field('LastSyncDate', 'string'),
                Field('LastRegistrationCode', 'integer'),
                Field('LastRegistrationCodeTime', 'datetime'),
                Field('created_on', 'datetime', default=request.now),
                Field('updated_on', 'datetime', default=request.now)
                )

db.define_table('ScheduleType',
                Field('ScheduleName', 'string'))

db.ScheduleType.update_or_insert(ScheduleName="Recurring")
db.ScheduleType.update_or_insert(ScheduleName="Once")

db.define_table('OccurType',
                Field('OccurType', 'string'))

db.OccurType.update_or_insert(OccurType="Hour")
db.OccurType.update_or_insert(OccurType="Daily")
db.OccurType.update_or_insert(OccurType="Weekly")

db.define_table('Schedule',
                Field('SchedulingType', db.ScheduleType),
                Field('IsEnabled', default=True),
                Field('Occurs', db.OccurType),
                Field('RecurringHourFrequency', 'integer'),
                Field('RecurringDailyFrequency', 'integer'),
                Field('RecurringWeeklyFrequency', 'integer'),
                Field('RecurringSunday', 'boolean'),
                Field('RecurringMonday', 'boolean'),
                Field('RecurringTuesday', 'boolean'),
                Field('RecurringWednesday', 'boolean'),
                Field('RecurringThursday', 'boolean'),
                Field('RecurringFriday', 'boolean'),
                Field('RecurringSaturday', 'boolean'),
                Field('OccursOnceAt', 'time'),
                Field('OccursXEveryHours', 'integer'),
                Field('StartingAt', 'time'),
                Field('EndingAt', 'time'),
                Field('StartingDate', 'date'),
                Field('EndingDate', 'date'),
                Field('Description', 'text'),
                Field('created_on', 'datetime', default=request.now),
                Field('updated_on', 'datetime', default=request.now))

db.define_table('Device',
                Field('DeviceSerialNumber', 'string', unique=True),
                Field('RegisteredUser', db.User),
                Field('RegisteredBy', db.User),
                Field('DetectMotion', 'boolean'),
                Field('DetectCapRotation', 'boolean'),
                Field('RegisteredOn', 'datetime', default=request.now),
                Field('UnRegisteredBy', db.User),
                Field('UnRegisteredOn' 'datetime'))

db.define_table('RelationshipType',
                Field('Name', 'string'))

db.RelationshipType.update_or_insert(Name='Son')
db.RelationshipType.update_or_insert(Name='Daughter')
db.RelationshipType.update_or_insert(Name='Father')
db.RelationshipType.update_or_insert(Name='Mother')
db.RelationshipType.update_or_insert(Name='Brother')
db.RelationshipType.update_or_insert(Name='Sister')
db.RelationshipType.update_or_insert(Name='Doctor')
db.RelationshipType.update_or_insert(Name='Clinic')
db.RelationshipType.update_or_insert(Name='Others')

db.define_table('CareShare',
                Field('PrimaryUser', db.User),
                Field('CareTakerID', db.User),
                Field('RelationShip', db.RelationshipType),
                Field('OtherRelationShip', 'string'),
                Field('NeedDailyReport', 'boolean'),
                Field('NeedScheduledAlert', 'boolean'))

db.define_table('Medication',
                Field('PatientID', db.User),
                Field('MedicationName', 'string'),
                Field('ScheduleID', db.Schedule),
                Field('TotalQuantity', 'integer'),
                Field('Dosage', 'string'),
                Field('IsRefill', 'boolean'),
                Field('RefillQuantity', 'integer'),
                Field('ExpiryDate', 'date'),
                Field('RefillReminder', 'date'),
                Field('photos', 'boolean'),
                Field('IsActive', 'boolean', default=True),
                Field('RemainingMedication', 'integer'))

db.define_table('MedicationAdjustments',
                Field('MedicationID', db.Medication),
                Field('AdjustedQuantity', 'integer'),
                Field('Reason', 'text'),
                Field('RemainingMedication', 'integer'))

db.define_table('MedicationHistory',
                Field('MedicationID', db.Medication),
                Field('TimeOfMedication', 'datetime'),
                Field('OnScheduleRange', 'boolean'),
                Field('RemainingMedication', 'integer')
                )

#
# if not request.env.web2py_runtime_gae:
#     # ---------------------------------------------------------------------
#     # if NOT running on Google App Engine use SQLite or other DB
#     # ---------------------------------------------------------------------
#     db = DAL(myconf.get('db.uri'),
#              pool_size=myconf.get('db.pool_size'),
#              migrate_enabled=myconf.get('db.migrate'),
#              check_reserved=['all'])
# else:
#     # ---------------------------------------------------------------------
#     # connect to Google BigTable (optional 'google:datastore://namespace')
#     # ---------------------------------------------------------------------
#     db = DAL('google:datastore+ndb')
#     # ---------------------------------------------------------------------
#     # store sessions and tickets there
#     # ---------------------------------------------------------------------
#     session.connect(request, response, db=db)
#     # ---------------------------------------------------------------------
#     # or store session in Memcache, Redis, etc.
#     # from gluon.contrib.memdb import MEMDB
#     # from google.appengine.api.memcache import Client
#     # session.connect(request, response, db = MEMDB(Client()))
#     # ---------------------------------------------------------------------

# # -------------------------------------------------------------------------
# # by default give a view/generic.extension to all actions from localhost
# # none otherwise. a pattern can be 'controller/function.extension'
# # -------------------------------------------------------------------------
# response.generic_patterns = ['*'] if request.is_local else []
# # -------------------------------------------------------------------------
# # choose a style for forms
# # -------------------------------------------------------------------------
# response.formstyle = myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
# response.form_label_separator = myconf.get('forms.separator') or ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------
#
from gluon.tools import Auth
#
# # host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db)
# service = Service()
# plugins = PluginManager()

# # -------------------------------------------------------------------------
# # create all tables needed by auth if not custom tables
# # -------------------------------------------------------------------------
auth.define_tables(username=False, signature=True)

# # -------------------------------------------------------------------------
# # configure email
# # -------------------------------------------------------------------------
# mail = auth.settings.mailer
# mail.settings.server = 'logging' if request.is_local else myconf.get('smtp.server')
# mail.settings.sender = myconf.get('smtp.sender')
# mail.settings.login = myconf.get('smtp.login')
# mail.settings.tls = myconf.get('smtp.tls') or False
# mail.settings.ssl = myconf.get('smtp.ssl') or False

# # -------------------------------------------------------------------------
# # configure auth policy
# # -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)
